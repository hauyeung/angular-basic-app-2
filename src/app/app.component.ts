import { Component } from '@angular/core';
import { Person } from './person';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  persons: Person[];

  constructor() {
    this.persons = [
      { name: 'Jane' },
      { name: 'John' },
      { name: 'Mary' },
    ]
  }
}